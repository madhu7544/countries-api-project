import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import Country from "./Country";
import ErrorPage from "./ErrorPage";
import { ThemeContext } from "../App";

const Home = () => {
  const [countries, setCountries] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("all");
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(null);
  const isDarkMode = useContext(ThemeContext);
  const [selectedLanguage, setSelectedLanguage] = useState("all");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("https://restcountries.com/v3.1/all");
        const countriesData = response.data.map((each) => ({
          id: each.cca3,
          flag: each.flags.png,
          countryName: each.name.common,
          population: each.population,
          region: each.region,
          capital: each.capital,
          languages: each?.languages || "N/A",
        }));
        setCountries(countriesData);
        setIsLoading(false);
        setHasError(null);
      } catch (error) {
        setIsLoading(false);
        setHasError(error.message);
      }
    };
    fetchData();
  }, []);

  const searchCountry = (event) => {
    setSearchValue(event.target.value);
  };

  const onSelectRegion = (event) => {
    setSelectedRegion(event.target.value);
  };

  const onSelectLanguage = (event) => {
    setSelectedLanguage(event.target.value);
  };

  const languageData = countries.reduce((acc, country) => {
    if (country.languages) {
      acc.push(...Object.values(country.languages));
     
      console.log((Object.values(country.languages)))
    }
    return acc;
  }, []);

 
  let languageArray = [];
  for (let i = 0; i < languageData.length;i++) {
    if (!languageArray.includes(languageData[i])){
      languageArray.push(languageData[i]);
    }
  }


 


  let filteredCountries = countries.filter((each) => {
    
    const searchMatch = each.countryName
      .toLowerCase()
      .startsWith(searchValue.toLowerCase());
    const regionMatch =
      selectedRegion === "all" || each.region === selectedRegion;
    const languageMatch = selectedLanguage ==="all" || Object.values(each.languages).includes(selectedLanguage) ;
    return searchMatch && (regionMatch && languageMatch);
  });



  const isFiltered = () => {
    if (filteredCountries.length > 0) {
      return filteredCountries.map((each) => (
        <Country
          key={each.id}
          countryId={each.id}
          flag={each.flag}
          countryName={each.countryName}
          population={each.population}
          region={each.region}
          capital={each.capital}
          isDarkMode={isDarkMode}
        />
      ));
    } else {
      return (
        <div className="no-error-message">
          <h1 className="error">No Countries Available</h1>
        </div>
      );
    }
  };

  return hasError ? (
    <ErrorPage />
  ) : (
    <div className={`main-container ${isDarkMode ? "main-dark" : "light"}`}>
      <div className="search-country-region-container">
        <div className="seacrh-container">
          <i className="fa fa-search search-icon"></i>
          <input
            type="search"
            placeholder="Search for Country..."
            className={`search-input ${isDarkMode ? "dark" : "inputs-search"}`}
            onChange={searchCountry}
          ></input>
        </div>
        <div className="region-container">
          <select
            id="regions"
            name="region"
            onChange={onSelectRegion}
            className={`region-search ${isDarkMode ? "dark" : "inputs-search"}`}
          >
            <option value="all">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">America</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>

        <select
          id="languages"
          name="language"
          onChange={onSelectLanguage}
          className={`region-search ${isDarkMode ? "dark" : "inputs-search"}`}
        >
          <option vlaue="all">All</option>
          {languageArray.map((each) => (
            <option key={each} value={each}>{each}</option>
          ))}
        </select>
      </div>
      <ul className="countries-container">
        {!isLoading ? isFiltered() : <h1>Loading...</h1>}
      </ul>
    </div>
  );
};

export default Home;
