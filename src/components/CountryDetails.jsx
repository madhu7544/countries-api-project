import React, { useEffect, useState,useContext } from "react";
import axios from "axios";
import { useParams, Link } from "react-router-dom";
import ErrorPage from "./ErrorPage";
import { ThemeContext } from "../App";


const CountryDetails = () => {
  const { id } = useParams();
  const [countryDetails, setCountryDetails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(null);

  const isDarkMode = useContext(ThemeContext);

  useEffect(() => {
    const getCountryData = async () => {
      try {
        const response = await axios.get(
          `https://restcountries.com/v3.1/alpha/${id}`
        );
        const countryData = {
          id: response.data[0].ccn3,
          flag: response.data[0].flags.png,
          countryName: response.data[0].name.common,
          population: response.data[0].population,
          region: response.data[0].region,
          subRegion: response.data[0]?.subregion,
          capital: response.data[0]?.capital,
          topLevelDomain: response.data[0]?.tld,
          currency: response.data[0]?.currencies,
          languages: response.data[0]?.languages,
          nativeName: response.data[0].name?.nativeName,
          borderCountry: response.data[0]?.borders,
        };

        setCountryDetails(countryData);
        setIsLoading(false);
        setHasError(null);
      } catch (error) {
        setIsLoading(false);
        setHasError(error.message);
      }
    };
    getCountryData();
  }, [id]);

  const border = () => {
    return countryDetails.borderCountry.map((each) => (
      <Link
        key={each}
        to={`/alpha/${each}`}
        className={`border ${isDarkMode ? "dark" : "border"}`}
      >
        {each}
      </Link>
    ));
  };

  return hasError ? (
    <ErrorPage />
  ) : (
    <div
      className={`main-details-container ${isDarkMode ? "main-dark" : "light"}`}
    >
      {!isLoading ? (
        <div className="country-details-container">
          <Link to="/" className="back-btn">
            <div
              className={`back-btn-container ${
                isDarkMode ? "main-dark" : "light"
              }`}
            >
              <i className="fa-light fa-arrow-left"></i>
              <span className="back-btn-span">Back</span>
            </div>
          </Link>
          <div
            key={countryDetails.countryName}
            className={`country-container ${
              isDarkMode ? "main-dark" : "light"
            }`}
          >
            <img
              src={countryDetails.flag}
              alt="country-flag"
              className="flag-image"
            />
            <div className="details-container">
              <h3 className="country-details-name">
                {countryDetails.countryName}
              </h3>
              <div className="country-data">
                <div className="data-1">
                  <p className="details-head">
                    Native Name:{" "}
                    <span>
                      {countryDetails.nativeName
                        ? Object.values(
                            Object.values(countryDetails.nativeName)
                          )[0].common
                        : "N/A"}
                    </span>
                  </p>
                  <p className="details-head">
                    Population: <span>{countryDetails.population}</span>
                  </p>
                  <p className="details-head">
                    Region: <span>{countryDetails.region}</span>
                  </p>
                  <p className="details-head">
                    Sub Region:{" "}
                    <span>
                      {countryDetails.subRegion
                        ? countryDetails.subRegion
                        : "N/A"}
                    </span>
                  </p>
                  <p className="details-head">
                    Capital:{" "}
                    <span>
                      {countryDetails.capital
                        ? countryDetails.capital[0]
                        : "N/A"}
                    </span>
                  </p>
                </div>
                <div className="data-2">
                  <p className="details-head">
                    Top Level Domain:{" "}
                    <span>
                      {countryDetails.topLevelDomain
                        ? countryDetails.topLevelDomain[0]
                        : "N/A"}
                    </span>
                  </p>
                  <p className="details-head">
                    Curriencies:{" "}
                    <span>
                      {countryDetails.currency
                        ? Object.values(
                            Object.values(countryDetails.currency)
                          )[0].name
                        : "N/A"}
                    </span>
                  </p>
                  <p className="details-head">
                    Languages:{" "}
                    <span>
                      {countryDetails.languages
                        ? Object.values(countryDetails.languages).join(", ")
                        : "N/A"}
                    </span>
                  </p>
                </div>
              </div>
              <div className="details-head-border">
                Border Countries:{" "}
                {countryDetails.borderCountry ? border() : "N/A"}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <h1 className="loading">Loading...</h1>
      )}
    </div>
  );
};

export default CountryDetails;
