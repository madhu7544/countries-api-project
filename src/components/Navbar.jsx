import { useContext } from "react";
import { ThemeContext } from "../App";


const Navbar = (props) => {
  const { toggleMode} =props;
  const isDarkMode = useContext(ThemeContext);

  return (
    <div className={`nav-bar ${isDarkMode ? 'dark' :'navbar-light'}`}>
      <h1 className="title-logo">Where in the World?</h1>
      <div className="dark-light-mode" onClick={toggleMode}>
      <i className="fa-solid fa-moon"></i>
        <p className="mode">{isDarkMode ? 'Light Mode' : 'Dark Mode'}</p>
      </div>
    </div>
  );
};

export default Navbar;