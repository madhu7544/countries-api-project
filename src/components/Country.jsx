import React from "react";
import { Link } from "react-router-dom";

const Country = (props) => {
  const {countryId, flag, countryName, population, region, capital,isDarkMode } = props;

  return (
    <li className= {`each-county-container ${isDarkMode ? 'dark' : 'light'}`}>
      <Link to={`/alpha/${countryId}`} className="each-country-data">
        <img src={flag} alt="flag" className="flag" />
        <div className={`country-details ${isDarkMode ? 'dark' : 'light'}`}>
          <h3>{countryName}</h3>
          <div className="details">
            <p className="details-head">
              Population :<span>{population}</span>
            </p>
            <p className="details-head">
              Region :<span>{region}</span>
            </p>
            <p className="details-head">
              Capital:<span>{capital}</span>
            </p>
          </div>
        </div>
      </Link>
    </li>
  );
};
export default Country;
