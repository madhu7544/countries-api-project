import { Routes, Route } from "react-router-dom";
import { useState, createContext } from "react";
import Home from "./components/Home";
import CountryDetails from "./components/CountryDetails";
import "./App.css";
import Navbar from "./components/Navbar";



export const ThemeContext = createContext();

const App = () => {
  const [isDarkMode, setDarkMode] = useState(false);
  // Use Context to do it

  const toggleMode = () => {
    setDarkMode(!isDarkMode);
  };

  return (
    <ThemeContext.Provider value={isDarkMode}>
      <Navbar toggleMode={toggleMode} />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/alpha/:id" element={<CountryDetails />} />
      </Routes>
    </ThemeContext.Provider>

  );
};

export default App;


